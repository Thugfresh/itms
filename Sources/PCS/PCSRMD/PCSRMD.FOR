      PROGRAM PCSRMD
C
C**********************************************************************
C       PEI DEPARTMENT OF TRANSPORTATION AND PUBLIC WORKS
C          INTEGRATED TRAFFIC MONITORING SYSTEM (ITMS)
C                       VERSION 1.04
C
C               GEOPLAN CONSULTANTS INC.
C                       1993 04 06
C**********************************************************************
C
C       THIS PROGRAM IS A MODIFIED VERSION OF TRFC PROGRAM "TRF34"
C
C       **********  REVISION HISTORY  **********
C
C   REV.     DATE     DESCRIPTION
C   ====  ==========  ==================================================
C   1.00  1991 11 12  1. PROGRAMS CONVERTED FROM VS/9 TO ANSI 77 FORTRAN
C         1992 03 09  2. CUSTOM CHANGES MADE FOR ITMS REQUIREMENTS:
C                        1. ARRAY DIMENSIONS REDUCED FROM 100 TO 53
C         1992 04 08     2. PAUSE LOOP ADDED AT END OF PROGRAM
C                     BY:     GEOPLAN CONSULTANTS INC. - DAVE LOUKES
C
C   1.01  1992 08 07  1. ARRAY DIMENSIONS INCREASED TO 54
C                     BY:     GEOPLAN CONSULTANTS INC. - DAVE LOUKES
C
C   1.02  1992 09 30  1. ISSUE PAGE EJECT AT END OF PRINT FILE
C                     BY:     GEOPLAN CONSULTANTS INC. - DAVE LOUKES
C
C   1.03  1992 12 02  1. FILE "PERMCNTS.ASC" HAS BEEN REVISED TO INCLUDE
C                        THE YEAR OF COUNT DATA, WHICH IS NOW PRINTED IN
C                        THE PROGRAM HEADER MESSAGE.
C
C   1.04  1993 04 06  1. FILE "PERMCNT2.ASC" IS NO LONGER CREATED BY
C                        THIS PROGRAM, AS IT IS NOT USED BY ANY OTHER
C                        ITMS PROGRAM.
C
C       INPUT FILES:
C       ============
C
C               UNIT 5: "PERMCNTS.ASC" - ASCII INPUT FILE OF DAILY COUNT
C                       VOLUMES EXPORTED FROM CLIPPER EDITED HOURLY
C                       VOLUMES PCS DATABASE.
C
C       OUTPUT FILES:
C       =============
C
C               UNIT 6: ASCII PRINTFILE
C
C               UNIT 10: "PERMUPDT.ASC" - ASCII OUTPUT FILE CONTAINING
C                       ONLY CALCULATED DAILY COUNTS FOR MISSING DAYS.
C
C-----------------------------------------------------------------------
C    MAIN PROGRAM CALLING MULTIPLE REGRESSION SUBROUTINE 'RESTEM'
C    THIS PROGRAM USED TO DETERMINE VALUES FOR THE MISSED PERMANENT
C    COUNTER DATA WHICH HAS THUS FAR BEEN GIVEN A VALUE OF ZERO
C
C    THE VALUES CALCULATED FOR THE MISSED DATA AS WELL AS THE
C    GOOD DATA IS SAVED ON FORTRAN UNIT 9
C----------------------------------------------------------------
C
C       NEW ITMS DECLARATIONS ...
C
      CHARACTER*10 SYDATE                                               ITMS1.00
      CHARACTER*8  SYTIME                                               ITMS1.00
      CHARACTER*6  SITEID, OLDID, BLANK                                 ITMS1.00
      INTEGER      DAYWK, DAYCNT, EOF                                   ITMS1.00
      INTEGER      YEAR                                                 REV.1.03
      REAL         STATS (7,30)                                         ITMS1.00
C
C         DIMENSIONS OF ARRAYS 'X', 'W', 'R', 'JVECTR' AND 'NVECTR'
C         REDUCED FROM 100 TO 54 (FOR MAXIMUM NUMBER OF SPECIFIC
C         WEEKDAYS IN A YEAR ...
C               1992 03 09 - DKL
C
      DIMENSION X(54,30),W(54),XBAR(30),A(30,30),SIG(30),IVAR(29),      REV.1.01
     1  B(29),SB(29),R(54),JVECTR(54),NVECTR(54),Z(60,30)               REV.1.01
      COMMON /ARGS/X,N,NP1,MAXN,MAXNP1,W,IW,EFIN,EFOUT,XBAR,A,SIG,CONST,ITMS1.00
     1  NVAR,FLEVEL,SY,NOIN,IVAR,B,SB,R,IND                             ITMS1.00
C
      DATA  BLANK /'      '/                                            ITMS1.00
C
C       GET CURRENT DATE AND TIME FOR REPORT HEADINGS ...
C
      CALL DATE (SYDATE, SYTIME)                                        ITMS1.00
C
C       PRINT PROGRAM HEADER MESSAGE ...
C
      WRITE (*,2001) SYDATE, SYTIME                                     ITMS1.00
2001  FORMAT (//' INTEGRATED TRAFFIC MONITORING SYSTEM - ITMS'/         ITMS1.00
     1          '            VERSION 1.04'//                            REV.1.04
     2          ' REPLACE MISSING COUNT DAYS - PROGRAM PCSRMD'/         ITMS1.00
     3          ' DATE: ',A10,13X,'TIME: ',A8//)                        ITMS1.00            
C
C       DEFINE INPUT AND OUTPUT FILES ...
C
      OPEN (UNIT=5, FILE='PERMCNTS.ASC', FORM='FORMATTED', ERR=9000)    ITMS1.00
C     OPEN (UNIT=6, FILE='PCSRMD.PRT', FORM='FORMATTED', ERR=9010)      ITMS1.00
      OPEN (UNIT=6, FILE='PRN')
C     OPEN (UNIT=9, FILE='PERMCNT2.ASC', FORM='FORMATTED', ERR=9020)    REV.1.04
      OPEN (UNIT=10, FILE='PERMUPDT.ASC', FORM='FORMATTED', ERR=9030)   ITMS1.00
C
      MAXN=54                                                           REV.1.02
      MAXNP1=30
       PI=3.1415927
C
      NRECS = 0                                                         ITMS1.00
      OLDID = '      '                                                  ITMS1.00
      EOF = 0                                                           ITMS1.00
       MN=0
       K=0
       N=0
      NP1=9
      IW=0
       LIMIT=8
C
C     ALL CO-EFFICIENTS FOR ALL COUNTERS TO BE CALCULATED
C
      EFIN=0.0
      EFOUT=0.0
      NP=NP1-1
C      READ (5,1001,END=79) KTR,KWS,JWS,NWS,LOC1,LOC2,LOC3              ITMS1.00
C      GO TO 98                                                         ITMS1.00
C 101  READ (5,1001,END=80) KTR,KWS,JWS,NWS,LOC1,LOC2,LOC3              ITMS1.00
C
C       READ INPUT DATA ...
C
C       NOTE: YEAR OF DATA NOW READ FROM FILE ALSO - REV. 1.03
C
101   READ (5, 1001, END=80) SITEID, YEAR, DAYWK, JULDAY, DAYCNT        REV.1.03
1001  FORMAT (A6,I4,I1,I3,I5)                                           REV.1.03
      NRECS = NRECS + 1                                                 ITMS1.00
C
C       CHECK INPUT DATA FOR ERRORS ...
C
      IF (SITEID.NE.BLANK) GO TO 1010                                   ITMS1.00
         WRITE (*,9006) NRECS                                           ITMS1.0
9006     FORMAT (' *** ITMS ERROR 1.06 - BLANK SITE ID FIELD FOUND ',   ITMS1.00
     1           'FOR RECORD ',I5)                                      ITMS1.00
         GO TO 9900                                                     ITMS1.00
C
1010  IF (DAYWK.GT.0 .AND. DAYWK.LT.8) GO TO 1020                       ITMS1.00
         WRITE (*,9007) DAYWK, NRECS                                    ITMS1.0
9007     FORMAT (' *** ITMS ERROR 1.07 - INVALID DAY OF WEEK (',        ITMS1.00
     1           I1,') WITHIN RECORD ',I5)                              ITMS1.00
         GO TO 9900                                                     ITMS1.00
C
1020  IF (JULDAY.GT.0 .AND. JULDAY.LT.367) GO TO 1030                   ITMS1.00
         WRITE (*,9008) JULDAY, NRECS                                   ITMS1.0
9008     FORMAT (' *** ITMS ERROR 1.08 - INVALID JULIAN DAY (',         ITMS1.00
     1           I3,') WITHIN RECORD ',I5)                              ITMS1.00
         GO TO 9900                                                     ITMS1.00

1030  IF (SITEID.EQ.OLDID) GO TO 105                                    ITMS1.00
C
C               SET UP NEW SITE ...
C
         NSITES = NSITES + 1                                            ITMS1.00
         IF (NSITES.GT.1) GO TO 2                                       ITMS1.00
C
C                WRITE YEAR OF DATA AS PART OF HEADER - REV. 1.03
C
            WRITE (6,1002) SYDATE, NSITES, SYTIME, SITEID, YEAR         REV.1.03
1002        FORMAT ('1',16X,'PEI DEPARTMENT OF TRANSPORTATION AND ',    ITMS1.00
     1           'PUBLIC WORKS'/                                        ITMS1.00
     2           ' ',18X,'INTEGRATED TRAFFIC MONITORING SYSTEM - ITMS'//ITMS1.00
     3           ' DATE: ',A10,11X,'MISSING DAY REGRESSION STATISTICS', ITMS1.00
     4           11X,'PAGE',I5/                                         ITMS1.00
     5           ' TIME: ',A8,8X,'FOR PERMANENT COUNTER SITE ',A6,      REV.1.03
     6           '  YEAR ',I4//)                                        REV.1.03
            WRITE (*,2002) SITEID, YEAR                                 REV.1.04
2002        FORMAT('+===> PROCESSING DATA FOR COUNTER SITE ',A6,        REV.1.04
     1           '  YEAR ',I4)                                          REV.1.04
            OLDID = SITEID                                              ITMS1.00
            GO TO 102                                                   ITMS1.00
C            
  105  IF (KDWS.NE.DAYWK) GO TO 2                                       ITMS1.00
  102  JULIAN=JULDAY                                                    ITMS1.00
       NCNT=DAYCNT                                                      ITMS1.00
       KDWS=DAYWK                                                       ITMS1.00
       T=2.0*PI*JULIAN/365.
  103  MN=MN+1
       K=K+1
       NVECTR(K)=JULIAN
       Z(MN,1)=COS(T)
       Z(MN,2)=COS(2.*T)
       Z(MN,3)=SIN(T)
       Z(MN,4)=SIN(2.*T)
       Z(MN,5)=SIN(3.*T)
       Z(MN,6)=SIN(4.*T)
       Z(MN,7)=SIN(5.*T)
       Z(MN,8)=SIN(6.*T)
       Z(MN,9)=NCNT/1000.
       IF(NCNT) 101,101,104
  104  N=N+1
C
C       CHECK FOR MAXIMUM DAYS PER YEAR EXCEEDED ...
C
       IF (N.LE.MAXN) GO TO 97                                          ITMS1.00
          WRITE (*,9009) DAYWK                                          ITMS1.00
9009      FORMAT (' ***  ITMS ERROR 1.09 - MORE THAN 54 COUNTS FOR ',   REV.1.01
     1            'DAY OF WEEK ',I1)                                    ITMS1.00
          GO TO 9900                                                    ITMS1.00
C
   97  X(N,1)=COS(T)
       X(N,2)=COS(2.0*T)
       X(N,3)=SIN(T)
       X(N,4)=SIN(2.0*T)
       X(N,5)=SIN(3.0*T)
       X(N,6)=SIN(4.0*T)
       X(N,7)=SIN(5.0*T)
       X(N,8)=SIN(6.0*T)
       X(N,9)=NCNT/1000.
       JVECTR(N)=JULIAN
       GO TO 101
   80  EOF = 1                                                          ITMS1.00
      IF (NRECS.GT.0) GO TO 2                                           ITMS1.00
         WRITE (*,9005)                                                 ITMS1.00
9005     FORMAT (//' *** ITMS ERROR 1.05 - NO RECORDS FOUND IN FILE ',  ITMS1.00
     1           '"PERMCNTS.ASC" - CALCULATIONS TERMINATED')            ITMS1.00
         GO TO 79                                                       ITMS1.00
C                  
    2  IND=0
       ISTEP=-1
C
C       NOTE:  ARGUMENTS TO RESTEM NOW IN COMMON BLOCK /ARGS/
C       
7     CALL RESTEM                                                       ITMS1.00
C   7 CALL RESTEM (X,N,NP1,MAXN,MAXNP1,W,IW,EFIN,EFOUT,XBAR,A,SIG,CONST,ITMS1.00
C    1  NVAR,FLEVEL,SY,NOIN,IVAR,B,SB,R,IND)                            ITMS1.00
       IF (IND) 13,12,13
   13  ISTEP=ISTEP+1
       IF (ISTEP) 4,5,4
    5   MN=NP-1
    4   IF (NVAR) 8,10,10                                               ITMS1.00
    8  NVAR=-NVAR
   10   IF (ISTEP-LIMIT) 7,14,14
   14  IND=-1
       GO TO 7
C
12    CONTINUE                                                          ITMS1.00
C
C       SAVE DAILY COEFFICIENTS FOR SUMMARY SITE REPORT ...
C
      DO 200 IND = 1,NP                                                 ITMS1.00
         STATS (KDWS,IND+1) = B (IND)                                   ITMS1.00
200   CONTINUE                                                          ITMS1.00
      STATS (KDWS,1) = CONST                                            ITMS1.00                
C
C     REPLACE ONLY MISSING COUNTS WITH THEORETICAL VALUES
C
        DO 20 I=1,K
        IF(Z(I,9)-0) 16,16,15
   16   COUNT=CONST
        DO 17 LJ=1,8
        COUNT=COUNT+B(LJ)*Z(I,LJ)
   17   CONTINUE
        GO TO 21                                                        ITMS1.00
   15   COUNT = Z(I,9)
   21   NCOUNT=COUNT*1000
C  20   WRITE (9,1030) KR,NVECTR(I),NCOUNT,L1,L2,L3
C
C       REMOVE WRITE TO FILE 9 (PERMCNT2.ASC) - REV. 1.04 - DKL
C
C     WRITE (9, 1001) OLDID, KDWS, NVECTR(I), NCOUNT                    REV.1.04
C
C               WRITE OUT ANY COMPUTED COUNTS TO SEPARATE FILE ...
C
      IF (Z(I,9).NE.0) GO TO 20                                         ITMS1.00
         WRITE (10,1003) OLDID, NVECTR(I), NCOUNT                       ITMS1.00
1003     FORMAT (A6,I3,I5)                                              ITMS1.00
         WRITE (6,1004) KDWS, NVECTR(I), NCOUNT                         ITMS1.00
1004     FORMAT (' FOR WEEKDAY',I2,' JULIAN DAY',I4,                    ITMS1.00
     1           ', CALCULATED DAILY VOLUME IS',I7)                     ITMS1.00         
20    CONTINUE                                                          ITMS1.00
C
        K=0
        MN=0
        N=0
       IF (OLDID.EQ.SITEID .AND. EOF.EQ.0) GO TO 102                    ITMS1.00
         WRITE (6,1005) OLDID                                           ITMS1.00
1005     FORMAT (///' ',16X,'REGRESSION COEFFICIENTS FOR PCS SITE ',A6//ITMS1.00
     1           ' ',13X,8(2X,'COEFF.')/                                ITMS1.00
     2           ' DAY CONSTANT     1',7X,'2',7X,'3',7X,'4',7X,'5',7X,  ITMS1.00
     3           '6',7X,'7',7X,'8')                                     ITMS1.00
         DO 300 IND = 1,7                                               ITMS1.00
            WRITE (6,1006) IND, (STATS(IND,NC), NC = 1,NP+1)            ITMS1.00
1006        FORMAT (' ',I2,F10.4,8F8.4)                                 ITMS1.00
300      CONTINUE                                                       ITMS1.00
C
         IF (EOF.EQ.1) GO TO 79                                         ITMS1.00
            WRITE (6,1002) SYDATE, NSITES, SYTIME, SITEID, YEAR         REV.1.03
            WRITE (*,2002) SITEID, YEAR                                 REV.1.04
            OLDID = SITEID                                              ITMS1.00
            GO TO 102                                                   ITMS1.00
C
C       *********************************************************
C       *       ENTER HERE FOR FILE ERROR HANDLING              *
C       *********************************************************
C
9000  WRITE (*,9001)                                                    ITMS1.00
9001  FORMAT (//' *** ITMS ERROR 1.01: CANNOT OPEN INPUT FILE ',        ITMS1.00
     1        '"PERMCNTS.ASC"')                                         ITMS1.00
      GO TO 9900                                                        ITMS1.00
C
9010  WRITE (*,9002)                                                    ITMS1.00
9002  FORMAT (//' *** ITMS ERROR 1.02: CANNOT OPEN LISTING FILE ',      ITMS1.00
     1        '"PERMCNTS.PRT"')                                         ITMS1.00
      GO TO 9900                                                        ITMS1.00
C
C       *** ERROR MESSAGE 1.03 REMOVED - REV. 1.04 - DKL
C
C9020  WRITE (*,9003)                                                   REV.1.04
C9003  FORMAT (//' *** ITMS ERROR 1.03: CANNOT OPEN OUTPUT FILE ',      REV.1.04
C     1        '"PERMCNT2.ASC"')                                        REV.1.04
C      GO TO 9900                                                       REV.1.04
C
9030  WRITE (*,9004)                                                    ITMS1.00
9004  FORMAT (//' *** ITMS ERROR 1.04: CANNOT OPEN OUTPUT FILE ',       ITMS1.00
     1        '"PERMUPDT.ASC"')                                         ITMS1.00
C
9900  CONTINUE
      WRITE (*,9999)                                                    ITMS1.00
9999  FORMAT ('    RUN TERMINATED'//)                                   ITMS1.00
C
C       PAUSE TO ALLOW SCANNING OF ERROR MESSAGES ...
C
79    DO 10000 I = 1,1000000
10000 CONTINUE
C
C       ISSUE PAGE EJECT ON PRINT FILE ...
C
      WRITE (6, 10001)                                                  REV.1.02
10001 FORMAT ('1')                                                      REV.1.02
C
       STOP 'DONE ...'
       END
C
C
      SUBROUTINE RESTEM                                                 ITMS1.00
C     SUBROUTINE RESTEM (X,N,NP1,MAXN,MAXNP1,W,IW,EFIN,EFOUT,XBAR,A,SIG,ITMS1.00
C    1 CONST,NVAR,FLEVEL,SY,NOIN,IVAR,B,SB,R,IND)                       ITMS1.00
C
C**********************************************************************
C       PEI DEPARTMENT OF TRANSPORTATION AND PUBLIC WORKS
C          INTEGRATED TRAFFIC MONITORING SYSTEM (ITMS)
C                       VERSION 1.0
C
C               GEOPLAN CONSULTANTS INC.
C                       1991 11 12
C**********************************************************************
C
C       THIS PROGRAM IS A MODIFIED VERSION OF TRFC PROGRAM "TRF34"
C
C       **********  REVISION HISTORY  **********
C
C   REV.     DATE     DESCRIPTION
C   ====  ==========  ==================================================
C   1.00  1991 11 12  1. PROGRAMS CONVERTED FROM VS/9 TO ANSI 77 FORTRAN
C                     2. CUSTOM CHANGES MADE FOR ITMS REQUIREMENTS
C                     BY:     GEOPLAN CONSULTANTS INC. - DAVE LOUKES
C
C------
C     MULTIPLE REGRESSION PROGRAM
C------
C
C         DIMENSIONS OF ARRAYS 'X', 'W', 'R', 'JVECTR' AND 'NVECTR'
C         REDUCED FROM 100 TO 54 (FOR MAXIMUM NUMBER OF SPECIFIC        REV.1.01
C         WEEKDAYS IN A YEAR ...
C               1992 03 09 - DKL
C
      DIMENSION X(54,30),W(54),XBAR(30),A(30,30),                       REV.1.01
     1 SIG(30),IVAR(29),B(29),SB(29),R(54)                              REV.1.01
      COMMON /ARGS/X,N,NP1,MAXN,MAXNP1,W,IW,EFIN,EFOUT,XBAR,A,SIG,CONST,ITMS1.00
     1  NVAR,FLEVEL,SY,NOIN,IVAR,B,SB,R,IND                             ITMS1.00
C
C       ERROR EXIT IF ONLY ONE ENTRY IN DATA ARRAY ...
C
      IF (N.GT.1) GO TO 50                                              ITMS1.00
         WRITE (*,1)                                                    ITMS1.00
1        FORMAT (//' *** ITMS ERROR 1.05 - ONLY ONE COUNT DAY IN ',     ITMS1.00
     1           'REGRESSION INPUT TO ROUTINE RESTEM'/                  ITMS1.00
     2           '    RUN TERMINATED - CHECK DAILY COUNT TOTALS')       ITMS1.00
         STOP                                                           ITMS1.00         
C------
C     IND=0 UPON FIRST CALL TO SUBRT.
C------
50    IF (IND) 175,100,160                                              ITMS1.00
  100 IND=1
C------
C     TEST IF WEIGHTS ARE INPUT. IF NOT, SET W(J)=1.0...WEIGHT SUM=N
C     IF INPUT, NORMALIZE WEIGHTS SO THAT AVERAGE OF WEIGHTS IS 1.0
C------
      IF (IW) 101,101,102
C------
C     NOT INPUT
C------
  101 DO 103 I=1,N
  103 W(I)=1.0
      GO TO 104
C------
C     INPUT
C------
  102 TEMP=0.0
      DO 105 I=1,N
  105 TEMP=TEMP+W(I)
      TEMP=TEMP/N
      DO 106 I=1,N
  106 W(I)=W(I)/TEMP
C------
C     COMPUTE MEAN OF EACH VARIABLE=XBAR
C------
  104 DO 114 J=1,NP1
      XBAR(J)=0.0
      DO 115 I=1,N
  115 XBAR(J)=XBAR(J)+W(I)*X(I,J)
  114 XBAR(J)=XBAR(J)/N
C------
C     COMPUTE WEIGHTED RESIDUAL SUMS OF SQUARES AND CROSS PRODUCTS
C------
      DO 117 I=1,NP1
      DO 117 J=1,NP1
      A(I,J)=0.0
      DO 116 K=1,N
  116 A(I,J)=A(I,J)+W(K)*(X(K,I)-XBAR(I))*(X(K,J)-XBAR(J))
  117 CONTINUE
C------
C     COMPUTE STANDARD DEVIATION. SET DIAGONALS OF CORRELATION MTRX=1.
C     NORMALIZE,THEN EXPAND UPPER TRIANGULAR MATRIX TO FULL
C------
      DO 120 I=1,NP1
      SIG(I)=SQRT(A(I,I))
  120 A(I,I)=1.0
      NP=NP1-1
      DO 121 I=1,NP
      II=I+1
      DO 121 J=II,NP1
      A(I,J)=A(I,J)/(SIG(I)*SIG(J))
  121 A(J,I)=A(I,J)
C------
C     COMPUTE DEGREES OF FREEDOM
C------
      PHI=N-1.0
C------
C     INITIALIZATION PROCEDURE FOR DETERMINING MOST SIGNIFICANT VARIABLE
C     TO BE ADDED TO REGRESSION
C     COMPUTE STANDARD ERROR OF DEPENDENT VARIABLE
C------
  125 SY=SIG(NP1)*SQRT(A(NP1,NP1)/PHI)
C------
C     ZERO COEFFICIENTS ARRAY
C------
      DO 131 I=1,NP
  131 B(I)=0.0
      VMIN = -10.E34
      VMAX=0.0
      NMIN=0
      NMAX=0
      NOIN=0
      DO 150 I=1,NP
      IF (A(I,I).LE.1.E-6) GO TO 150                                    ITMS1.00
C------
C     COMPUTE VARIANCE
C------
      V=A(I,NP1)*A(NP1,I)/A(I,I)                                        ITMS1.00
      IF (V) 142,150,143
  143 IF (V-VMAX) 150,150,144
  144 VMAX=V
      NMAX=I
      GO TO 150
C------
C     X(I) IS IN REGRESSION...COMPUTE COEFFICIENT B AND STAND. ERROR OF
C     COEFF.
C------
  142 NOIN=NOIN+1
      IVAR(NOIN)=I
      B(NOIN)=A(I,NP1)*SIG(NP1)/SIG(I)
      SB(NOIN)=SY*SQRT(A(I,I))/SIG(I)
      IF (V-VMIN) 150,150,145
  145 VMIN=V
      NMIN=I
  150 CONTINUE
C------
C     COMPUTE CONSTANT
C------
      TEMP=0.0
      DO 151 I=1,NOIN
      II=IVAR(I)
  151 TEMP=TEMP+B(I )*XBAR(II)
      CONST=XBAR(NP1)-TEMP
C     RETURN                                                            ITMS1.00
      GO TO 9000                                                        ITMS1.00
C------
C     COMPUTE FLEVEL
C------
  160 FLEVEL=VMIN*PHI/A(NP1,NP1)
C------
C     COMPARE F LEVELS...
C------
      IF (EFOUT+FLEVEL) 153,153,152
  152 K=NMIN
      PHI=PHI-1.0
      NVAR=-K
      GO TO 200
  153 FLEVEL=VMAX*(PHI-1.)/(A(NP1,NP1)-VMAX)
       IF (EFIN-FLEVEL) 154,175,175
  154 K=NMAX
      PHI=PHI-1.0
      NVAR=K
C------
C     CALCULATE NEW MATRIX
C------
  200 DO 210 I=1,NP1
      IF (I-K) 230,210,230
  230 DO 240 J=1,NP1
      IF (J-K) 260,240,260
  260 A(I,J)=A(I,J) - A(I,K)*A(K,J)/A(K,K)
  240 CONTINUE
  210 CONTINUE
      DO 280 I=1,NP1
      IF (I-K) 300,280,300
  300 A(I,K)=-A(I,K)/A(K,K)
  280 CONTINUE
      DO 320 J=1,NP1
      IF (J-K) 340,320,340
  340 A(K,J)=A(K,J)/A(K,K)
  320 CONTINUE
  350 A(K,K)=1.0/A(K,K)
      GO TO 125
  175 DO 178 J=1,N
      TEMP=0.0
      DO 177 I=1,NOIN
      II=IVAR(I)
  177 TEMP=TEMP + B(I)*X(J,II)
  178 R(J)=CONST+TEMP
C------
C     SET INDICT. TO INDICATE END OF COMPUTATION HAS BEEN REACHED
C------
      IND=0
9000  RETURN                                                            ITMS1.00
      END
