      SUBROUTINE ANLPRT
C
C**********************************************************************
C       PEI DEPARTMENT OF TRANSPORTATION AND PUBLIC WORKS
C          INTEGRATED TRAFFIC MONITORING SYSTEM (ITMS)
C                       VERSION 1.03
C
C               GEOPLAN CONSULTANTS INC.
C                       1992 11 26
C**********************************************************************
C
C       **********  REVISION HISTORY  **********
C
C   REV.     DATE     DESCRIPTION
C   ====  ==========  ==================================================
C   1.00  1991 11 22  1. PROGRAMS CONVERTED FROM VS/9 TO ANSI 77 FORTRAN
C         1991 12 12  2. CUSTOM CHANGES MADE FOR ITMS REQUIREMENTS
C                     BY:     GEOPLAN CONSULTANTS INC. - DAVE LOUKES
C         1992 40 12    - REVISIONS MADE TO ALLOW THE USER TO SELECT
C                         THE AMOUNT OF PRINTED OUTPUT DESIRED
C
C   1.01  1992 05 11  INTERSECTION SITE ID CHANGED TO 8 CHARACTERS
C                       GEOPLAN CONSULTANTS INC. - DAVE LOUKES
C
C   1.02  1992 07 09  1. REFORMAT DESIGN SUMMARY REPORT TO PRINT 4 LEGS
C                        ON ONE PAGE.
C                     2. GENERATE A PAGE EJECT AT END OF PRINTER FILE.
C                     BY:     GEOPLAN CONSULTANTS INC. - DAVE LOUKES
C
C   1.03  1992 11 26  1. ICSANL REVISION NUMBER SET TO 1.03
C                       GEOPLAN CONSULTANTS INC. - DAVE LOUKES
C
C-----------------------------------------------------------------------
C
C       THIS SUBROUTINE PRINTS THE RESULTS FROM THE INTERSECTION
C       ANALYSIS. DATA REQUIRED FOR THE PRINTOUTS ARE CONTAINED 
C       WITHIN THE LABELLED COMMON AREAS.
C
C       THIS SUBROUTINE WAS INCLUDED FOR ITMS 1.00 IN ORDER TO BREAK
C       UP THE MAIN PROGRAM INTO CODE SEGMENTS WHICH WERE UNDER 64K IN
C       SIZE (A RESTRICTION OF THE MICROSOFT FORTRAN DOS ENVIRONMENT).
C
C-----------------------------------------------------------------------
C
C DECLARATIONS OF THE VARIABLES.
C
      IMPLICIT INTEGER (A-Z)
C
C       THE FOLLOWING INTEGER VARIABLE CONTROLS THE EXTENT OF THE
C       PRINTED OUTPUT TO BE GENERATED. IT IS SET IN THE MAIN PROGRAM
C       "ICSANL.FOR", AND IS PASSED TO SUBROUTINE "ANLPRT.FOR" THROUGH
C       LABELLED COMMON AREA "/INPUT1/".
C
C       THE ALLOWABLE VALUES FOR VARIABLE "PRTLVL" ARE:
C
C               1 = PRINT ALL RESULTS
C               2 = PRINT APPROACH AND DESIGN SUMMARIES ONLY
C               3 = PRINT ONLY DESIGN SUMMARIES
C
      INTEGER PRTLVL
C
      INTEGER YY,MM,DD,INDXNO
      INTEGER NINT,NC,INTER,STPT,ENDPT,I,MOVE(20)
      INTEGER T
      INTEGER CLASS(99,40,5), TOTCOL(5,40)
      INTEGER SINSUM(99,4,5)
      INTEGER SINTOT(4,5)
      INTEGER TEMP1
      INTEGER AADTOT(5)
      INTEGER AADTIN(5),TOAADT(5),TOTVT(5,10)
      INTEGER VTDIR1(5,10),VTDIR2(5,10),TAADTT(5)
      INTEGER AADTT1(5),AADTT2(5),LAGHR(5,10)
      INTEGER PERMON(5,4,10),POST(99,16,5)
      INTEGER POSTOT(16,5),TOTPOS(99,4,5),TOTALL(4,5)
      INTEGER FINTOT(99,5),GRDTOT(5)
      INTEGER INDRUN(5,10),KEY(10),COUNT,P,SPEED(5)
      INTEGER LAGOUT(5,10)
      INTEGER TOTADT(5),AADT(5)
      INTEGER FLAG
      INTEGER SUB, PLACE
      INTEGER PAGE
      INTEGER DAY
      INTEGER INFILE
      INTEGER HRFILE
      INTEGER MNFILE
      INTEGER PRTFIL
      INTEGER ADTFIL
C
      REAL    TIME(99)
      REAL    ETIME(99),SPTIME(10),ESPTIM(10)
      REAL    PERAPP(4,5)
      REAL    DIR1(5,10),DIR2(5,10),DIRTOT(5,10)
      REAL    PHF(5,10),INDTRK(5,10),MOVPER(5,4,10)
      REAL    MADTFC(5,12)
C
      LOGICAL FOUND
C
      CHARACTER*4  LOC(20), W1(2), CREW(3), DOW(3), STREET(5,5)
      CHARACTER*4  MOVER(20,3)
      CHARACTER*4  MOVES(5,6)
      CHARACTER*4  TURN(5,3)
      CHARACTER*6  SECTID(5)
      CHARACTER*6  PCSLEG(5)
      CHARACTER*8  SITEID
      CHARACTER*10 SYDATE
      CHARACTER*8  SYTIME
C
C       LABELLED COMMON DEFINITIONS 
C
      COMMON /FILES/  PRTFIL, INFILE, HRFILE, MNFILE, ADTFIL
      COMMON /INPUT1/ PRTLVL, NC, INTER, NINT, INDXNO, YY, MM, DD,
     1                FLAG, DAY, MOVE
      COMMON /INPUT2/ CLASS, TIME, SPEED, FOUND, ETIME
      COMMON /INPUT3/ SYDATE, SYTIME, LOC, CREW, DOW, W1, STREET, MOVER,
     1                MOVES, SITEID, TURN, SECTID, PCSLEG
      COMMON /OUTPT1/ SINSUM, SINTOT, PERMON, LAGHR, MOVPER,
     1                KEY, POST, POSTOT, TOTPOS, FINTOT, TOTALL,
     2                TOTCOL, GRDTOT, PERAPP, SUB, PLACE, PAGE
      COMMON /OUTPT2/ TOTADT, AADT, SPTIME, ESPTIM, LAGOUT, INDRUN,
     1                INDTRK, PHF, MADTFC
      COMMON /OUTPT3/ AADTIN, AADTT1, VTDIR1, DIR1, AADTOT, AADTT2,
     1                VTDIR2, DIR2, TOAADT, TAADTT, TOTVT, DIRTOT
C
C
C PRINT OUT THE TITLE PAGE ...
C
      PAGE = 1
      WRITE (PRTFIL, 1000) SYDATE, PAGE, SYTIME
1000  FORMAT ('1','DATE: ',A10,24X,'P.E.I. DEPARTMENT OF ',
     1        'TRANSPORTATION AND PUBLIC WORKS',32X,'PAGE ',I3/
     2        ' ','TIME: ',A8,30X,'INTEGRATED TRAFFIC MONITORING ',
     3        'SYSTEM - ITMS'/
     4        ' ',37X,'INTERSECTION COUNT ANALYSIS PROGRAM - ICSANL',
     5        ' (VERSION 1.03)')                                        REV.1.03
      WRITE (PRTFIL, 8020)
8020  FORMAT (//////' ',17X,100('*'),/' ',17X,'*',98X,'*')
      WRITE (PRTFIL, 2031)
2031  FORMAT (' ',17X,'*',35X,'INTERSECTION COUNT FOR',41X,'*')
      WRITE (PRTFIL, 8021)
8021  FORMAT (' ',17X,'*',98X,'*')
      WRITE (PRTFIL, 2032) (LOC(I),I=1,20)
2032  FORMAT (' ',17X,'*',9X,20A4,9X,'*')
      WRITE (PRTFIL, 8021)
      WRITE (PRTFIL, 2033) SITEID, MM,DD,YY
2033  FORMAT (' ',17X,'*',20X,'SITE ID: ',A8,25X,'DATE: ',I2,'/',I2,
     1        '/',I2,22X,'*')
      WRITE (PRTFIL, 8022)
8022  FORMAT (' ',17X,'*',98X,'*',/' ',17X,100('*'))
C
C THIS PART OF THE MAIN PROGRAM IS THE FORMATING AND PRINTING
C OF THE RESULTS FROM THE CALCULATIONS THAT WERE MADE.
C
      N=1
      T=0
C
C       ***  IF ONLY THE DESIGN SUMMARIES ARE TO BE PRINTED, SKIP THIS
C       ***  SECTION ...
C               1992 04 22 - DKL
C
      IF (PRTLVL .EQ. 3) GO TO 6480
C
      DO 648 L=1,NINT
        K=L
        STPT=1
        ENDPT=NC
        TEMP1=NINT-1
C
C PRINTING OUT OF THE DATA (COUNTS) FOR EACH APPROACHING
C STREET, THE TOTAL VOLUME FOR EACH PEAK HOUR OF EACH STREET,
C AND THE PERCENTAGE OF THAT VOLUME COMPARED TO THE TOTAL
C VOLUME FOR THE DAY OF THAT APPROACHING STREET.
C
C
C       ***  IF ONLY THE APPROACH AND DESIGN SUMMARIES ARE TO BE
C       ***  PRINTED, SKIP THISSECTION ...
C               1992 04 22 - DKL
C
      IF (PRTLVL .GT. 1) GO TO 6000
C
        DO 600 J=1,TEMP1
          T=T+1
          K=K+1
          COUNT=0
          S=1
          IF (K .LE. NINT) GO TO 610
            K=1
610       PAGE = PAGE + 1
          WRITE (PRTFIL, 1000) SYDATE, PAGE, SYTIME
          WRITE (PRTFIL, 1010)
1010      FORMAT (//' ',54X,'LEG MOVEMENT SUMMARY')
          WRITE (PRTFIL, 1001) SITEID, (LOC(I),I=1,20),YY,MM,DD,
     1           (CREW(I),I=1,3),(DOW(I),I=1,3),(W1(I),I=1,2),
     2           INDXNO
1001      FORMAT(//,1X,'SITE ID: ',A8,3X,'LOCATION:',1X,20A4,
     1           //,1X,'DATE:',1X,2(I2,'/'),I2,8X,'CREW:',1X,3A4,8X,
     2           'D.O.W.:',1X,3A4,4X,'WEATHER:',1X,2A4,6X,'INDEX NO.: ',
     3           I5)
C
          DO 620 I=1,5
            IF (I .NE. MOVE(T)) GO TO 620
              TEMP=I
620       CONTINUE
          WRITE (PRTFIL, 1002) L, (STREET(L,I),I=1,5),SECTID(L),        ITMS1.00
     1           (MOVES(TEMP,I),I =1,6),(STREET(K,I),I=1,5), SECTID(K)  ITMS1.00
1002      FORMAT (//,1X,'APPROACH LEG ',I1,': ',5A4,3X,'SECTID #: ',A6, ITMS1.00
     1            10X,6A4,5A4,3X,'SECTID #: ',A6)                       ITMS1.00
C
          WRITE (PRTFIL, 1004)
1004      FORMAT ('0',4X,'TIME',19X,'CARS,PU',7X,'SU,TRK',8X,
     *            'TT3',/,28X,'VANS,MC',7X,'BUSES',9X,'4,5',7
     *            X,'TT6',8X,'TOTAL')
          DO 660 I=1,INTER
           IF (MOD(I,50).NE.0) GO TO 640
              PAGE = PAGE + 1
              WRITE (PRTFIL, 1000) SYDATE, PAGE, SYTIME
              WRITE (PRTFIL, 1004)
640        IF (I .EQ. KEY(S) .OR. FOUND) GO TO 661
            WRITE (PRTFIL, 1007) TIME(I),ETIME(I),(CLASS(I,K2,L),K2=    ITMS1.00
     *                     STPT,ENDPT),SINSUM(I,J,L)
1007        FORMAT (' ',F5.2,'-',F5.2,17X,I3,2(10X,I3),7X,I3,
     *              7X,I5)
            GO TO 660
661        FOUND=.TRUE.
           COUNT=COUNT+1
           GO TO (932,1935,1936, 1937),COUNT
1935          WRITE (PRTFIL, 1141) TIME(I),ETIME(I),(CLASS(I,K2,L),K2=  ITMS1.00
     *                      STPT,ENDPT),SINSUM(I,J,L),PERMON(
     *                      L,J,S)
1141         FORMAT (' ',F5.2,'-',F5.2,17X,I3,2(10X,I3),7X,I3
     *               ,7X,I5,5X,'*',2X,I4,1X,'TOTAL MOVEMENT')
             GO TO 972
1936        WRITE (PRTFIL, 4003) TIME(I),ETIME(I),(CLASS(I,K2,L),K2=    ITMS1.00
     *                     STPT,ENDPT),SINSUM(I,J,L),LAGHR(L,
     *                     S)
4003        FORMAT (' ',F5.2,'-',F5.2,17X,I3,2(10X,I3),7X,I3,
     *              7X,I5,5X,'*',2X,I4,1X,'TOTAL APPROACH')
            GO TO 972
C
1937        WRITE (PRTFIL, 4004) TIME(I),ETIME(I),(CLASS(I,K2,L),K2=    ITMS1.00
     *                     STPT,ENDPT),SINSUM(I,J,L),MOVPER(L
     *                     ,J,S),(TURN(TEMP,P),P=1,3)
4004        FORMAT (' ',F5.2,'-',F5.2,17X,I3,2(10X,I3),7X,I3,
     *              7X,I5,5X,'*',F6.2,1X,'GOES',1X,3A4)
            GO TO 972
932        WRITE (PRTFIL, 1021) TIME(I),ETIME(I),(CLASS(I,K2,L),K2=STPT,ITMS1.00
     *                    ENDPT),SINSUM(I,J,L)                          ITMS1.00
1021       FORMAT (' ',F5.2,'-',F5.2,17X,I3,2(10X,I3),7X,I3,7
     *             X,I5,5X,'*')
972        IF (COUNT .LT. 4) GO TO 660
             FOUND=.FALSE.
             COUNT=0
             S=S+1
660       CONTINUE
          S=1
C
          WRITE (PRTFIL, 1008) (TOTCOL(L,K2),K2=STPT,ENDPT),SINTOT(J,L) ITMS1.00
1008      FORMAT ('0',3X,'TOTAL',18X,2(I5,8X),I5,5X,I5,6X,I6)
          STPT=ENDPT+1
          ENDPT=ENDPT+NC
600     CONTINUE
C
6000    CONTINUE
C
C THE PRINTING OUT OF THE SUMMARY TABLE FOR THE APPROACHING
C STREET AND THE PERCENTAGES OF THE VOLUMES OF EACH MOVEMENT
C (TURNING DIRECTION ONTO ANOTHER STREET) COMPARED TO THE
C TOTAL VOLUME OF THE APPROACHING STREET FOR THE DAY.
C
       PAGE = PAGE + 1
       WRITE (PRTFIL, 1000) SYDATE, PAGE, SYTIME
       WRITE (PRTFIL, 1009) (STREET(L,I),I=1,5), SECTID(L)
1009      FORMAT ('0',20X,'SUMMARY TABLE FOR VEHICLES APPROACHING',
     *            ' FROM : ',5A4,5X,'SUBSECTION ID: ',A6)
       WRITE (PRTFIL, 1001) SITEID, (LOC(I),I=1,20),YY,MM,DD,
     1        (CREW(I),I=1,3), (DOW(I),I=1,3), (W1(I),I=1,2), INDXNO
C
       GO TO (9999,9999,613,614,616), NINT
613       TEMP=N+1
          WRITE (PRTFIL, 1022)((TURN((MOVE(T2)),I),I=1,3),T2=N,TEMP)    ITMS1.00
1022      FORMAT ('0',33X,2(3A4,20X),'TOTAL')
          GO TO 617                                                     ITMS1.00
614       TEMP=N+2
          WRITE (PRTFIL, 1023)((TURN((MOVE(T2)),I),I=1,3),T2=N,TEMP)    ITMS1.00
1023      FORMAT ('0',21X,3(3A4,20X),'TOTAL')
          GO TO 617
616       TEMP=N+3
          WRITE (PRTFIL, 1024)((TURN((MOVE(T2)),I),I=1,3),T2=N,TEMP)    ITMS1.00
1024      FORMAT ('0',17X,3(3A4,17X),3A4,11X,'TOTAL')
617       N=N+(NINT-1)
C
          GO TO (9999,9999,623,624,626), NINT
623       WRITE (PRTFIL, 1031)
1031      FORMAT (' ',25X,2('CAR/PU',3X,'SU/',5X,'TT',4X,'TO',          ITMS1.00
     *            'TAL',4X),/,4X,'TIME',18X,2('VAN/MC',3X,'B',          ITMS1.00
     *            'US',2X,'3,4,5,6',11X))
          GO TO 627
624       WRITE (PRTFIL, 1032)
1032      FORMAT (' ',16X,3('CAR/PU',3X,'SU/',5X,'TT',4X,'TO',          ITMS1.00
     *            'TAL',4X),/,4X,'TIME',9X,3('VAN/MC',3X,'BU',          ITMS1.00
     *            'S',2X,'3,4,5,6',11X))
          GO TO 627
626       WRITE (PRTFIL, 1033)
1033      FORMAT (' ',12X,4('CAR/PU',2X,'SU/',5X,'TT',4X,'TO',          ITMS1.00
     *            'TAL',2X),/,4X,'TIME',5X,4('VAN/MC',2X,'BU',          ITMS1.00
     *            'S',2X,'3,4,5,6',9X))
C
627       CONTINUE
          DO 633 I=1,INTER
             IF (MOD(I,50).NE.0) GO TO 628
                PAGE = PAGE + 1
                WRITE (PRTFIL, 1000) SYDATE, PAGE, SYTIME
628          IF (I .EQ. KEY(S) .OR. FOUND) GO TO 741
                GO TO (9999,9999,634,636, 637), NINT
634             WRITE (PRTFIL, 1038) TIME(I),ETIME(I),(POST(I,J,L),
     *                         J=1,3),TOTPOS(I,1,L),(POST(I,J
     *                         ,L),J=4,6),TOTPOS(I,2,L),FINTO
     *                         T(I,L)
1038            FORMAT (' ',F5.2,'-',F5.2,14X,2(2(I4,3X),I4,5
     *                  X,I4,5X),6X,I5)
                GO TO 633
636             WRITE (PRTFIL, 1039) TIME(I),ETIME(I),(POST(I,J,L),
     *                         J=1,3),TOTPOS(I,1,L),(POST(I,J
     *                         ,L),J=4,6),TOTPOS(I,2,L),(POST
     *                         (I,J,L),J=7,9),TOTPOS(I,3,L),F
     *                         INTOT(I,L)
1039            FORMAT (' ',F5.2,'-',F5.2,5X,3(2(I4,3X),2(I4,
     *                  5X)),5X,I5)
                GO TO 633
637             WRITE (PRTFIL, 1041) TIME(I),ETIME(I),(POST(I,J,L),
     *                         J=1,3),TOTPOS(I,1,L),(POST(I,J
     *                         ,L),J=4,6),TOTPOS(I,2,L),(POST
     *                         (I,J,L),J=7,9),TOTPOS(I,3,L),(
     *                         POST(I,J,L),J=10,12),TOTPOS(I,
     *                         4,L),FINTOT(I,L)
1041            FORMAT (' ',F5.2,'-',F5.2,2X,3(I4,2X,I4,3X,I4
     *                 ,5X,I4,3X),I4,2X,I4,3X,I4,5X,I4,1X,I5)
                GO TO 633
741           FOUND=.TRUE.
              GO TO (9999,9999,742,743,744), NINT
742           WRITE (PRTFIL, 1053) TIME(I),ETIME(I),(POST(I,J,L),J=
     *                       1,3),TOTPOS(I,1,L),(POST(I,J,L),
     *                       J=4,6),TOTPOS(I,2,L),FINTOT(I,L)
1053          FORMAT (' ',F5.2,'-',F5.2,'*',13X,2(2(I4,3X),2(
     *                I4,5X)),6X,I5)
              GO TO 746
743           WRITE (PRTFIL, 1054) TIME(I),ETIME(I),(POST(I,J,L),J=
     *                       1,3),TOTPOS(I,1,L),(POST(I,J,L),
     *                       J=4,6),TOTPOS(I,2,L),(POST(I,J,L
     *                       ),J=7,9),TOTPOS(I,3,L),FINTOT(I,
     *                       L)
1054          FORMAT (' ',F5.2,'-',F5.2,'*',4X,3(2(I4,3X),2(I
     *                4,5X)),5X,I5)
              GO TO 746
744           WRITE (PRTFIL, 1056) TIME(I),ETIME(I),(POST(I,J,L),J=
     *                       1,3),TOTPOS(I,1,L),(POST(I,J,L),
     *                       J=4,6),TOTPOS(I,2,L),(POST(I,J,L
     *                       ),J=7,9),TOTPOS(I,3,L),(POST(I,J
     *                       ,L),J=10,12),TOTPOS(I,4,L),FINTO
     *                       T(I,L)
1056          FORMAT (' ',F5.2,'-',F5.2,'*',1X,3(I4,2X,I4,3X,
     *               I4,5X,I4,3X),I4,2X,I4,3X,I4,5X,I4,1X,I5)
746           COUNT=COUNT+1
              IF (COUNT .LT. 4) GO TO 633
                COUNT=0
                S=S+1
                FOUND=.FALSE.
633           CONTINUE
C
          GO TO (9999,9999,756, 757,758), NINT
756       WRITE (PRTFIL, 1062) (POSTOT(I,L),I=1,3),TOTALL(1,L),(POS
     *                   TOT(I,L),I=4,6),TOTALL(2,L),GRDTOT(L
     *                   )
1062      FORMAT ('0',3X,'TOTAL',16X,2(I5,2(2X,I5),4X,I5,4X),
     *            6X,I6)
          WRITE (PRTFIL, 1071) (PERAPP(I,L),I=1,2)
1071      FORMAT (' ','% OF APPROACH',33X,F6.2,26X,F6.2)
          GO TO 648
757       WRITE (PRTFIL, 1063) (POSTOT(I,L),I=1,3),TOTALL(1,L),(POS
     *                   TOT(I,L),I=4,6),TOTALL(2,L),(POSTOT(
     *                   I,L),I=7,9),TOTALL(3,L),GRDTOT(L)
1063      FORMAT ('0',3X,'TOTAL',7X,3(I5,2(2X,I5),4X,I5,4X),5
     *            X,I6)
          WRITE (PRTFIL, 1072) (PERAPP(I,L),I=1,3)
1072      FORMAT (' ','% OF APPROACH',24X,F6.2,2(26X,F6.2))
          GO TO 648
758       WRITE (PRTFIL, 1064) (POSTOT(I,L),I=1,3),TOTALL(1,L),(POS
     *                   TOT(I,L),I=4,6),TOTALL(2,L),(POSTOT(
     *                   I,L),I=7,9),TOTALL(3,L),(POSTOT(I,L)
     *                   ,I=10,12),TOTALL(4,L),GRDTOT(L)
1064      FORMAT ('0',3X,'TOTAL',4X,3(I5,1X,I5,2X,I5,4X,I5,2X
     *            ),I5,1X,I5,2X,I5,4X,I5,I6)
          WRITE (PRTFIL, 1073) (PERAPP(I,L),I=1,4)
1073      FORMAT (' ','% OF APPROACH',20X,F6.2,3(23X,F6.2))
C
648     CONTINUE
C
6480    CONTINUE
C
C THE PRINTING OUT OF THE INTERSECTION DESIGN SUMMARY, WHICH
C SHOWS THE FACTORED AVERAGE DAILY TRAFFIC, ANNUAL AVERAGE
C DAILY TRAFFIC, POSTED SPEED, PEAK HOURS, PEAK HOURS VOLUMES
C ,PEAK HOURS TWO WAY VOLUME,PEAK HOURS PERCENTAGE OF TRUCKS,
C THE PEAK HOUR FACTOR FOR EACH PEAK HOUR, THE AMOUNT OF
C MOVEMENT FOR EACH PEAK HOUR, AND THE PERCENTAGE OF THAT
C MOVEMENT COMPARED TO THE VOLUME OF VEHICLES IN THAT PEAK
C HOUR, FOR EACH APPROACHING STREET.
C
        T=1
        PAGE = PAGE + 1
        WRITE (PRTFIL, 1000) SYDATE, PAGE, SYTIME
        WRITE (PRTFIL, 1082)
1082    FORMAT ('0',54X,'INTERSECTION DESIGN SUMMARY')                  REV.1.02
        WRITE (PRTFIL, 1001) SITEID, (LOC(J),J=1,20),YY,MM,DD,
     1         (CREW(J),J=1,3), (DOW(J),J=1,3), (W1(I),I=1,2),
     *                 INDXNO
        DO 921 L=1,NINT
         IF (L.NE.5) GO TO 9210                                         REV.1.02
              PAGE = PAGE + 1
              WRITE (PRTFIL, 1000) SYDATE, PAGE, SYTIME
              WRITE (PRTFIL, 1082)
              WRITE (PRTFIL, 1001) SITEID, (LOC(J),J=1,20),YY,MM,DD,
     1               (CREW(J),J=1,3), (DOW(J),J=1,3), (W1(I),I=1,2),
     2                 INDXNO
9210      WRITE (PRTFIL, 1081) L, (STREET(L,J),J=1,5), SECTID(L),
     1           PCSLEG(L), SPEED(L), TOTADT(L), AADT(L)
1081      FORMAT ('0','APPROACH LEG ',I1,': ',5A4,3X,'SECTID: ',        REV.1.02
     1            A6,3X,'PCS MATCH: ',A6,3X,'POSTED SPEED: ',I3,3X,
     2            'FACTORED ADT: ',I6,3X,'AADT: ',I6)
C
          GO TO (9999,9999,771,772,773), NINT
771       WRITE (PRTFIL, 1083) (TURN((MOVE(T)),P),P=1,3),(TURN((MOV
     *                   E(T)),P),P=1,3),(TURN((MOVE(T+1)),P)
     *                   ,P=1,3),(TURN((MOVE(T+1)),P),P=1,3)
1083      FORMAT ('0',12X,'TIME',7X,'APPROACH VOLUME',
     *            2X,2('#',3A4,3X,'%',3A4,3X),2X,'%TRKS',4X,
     *            'PHF',/,25X,'IN',2X,'OUT',2X,'TOTAL')
          GO TO 774
772       WRITE (PRTFIL, 1084) (TURN((MOVE(T)),P),P=1,3),(TURN((MOV
     *                   E(T)),P),P=1,3),(TURN((MOVE(T+1)),P)
     *                   ,P=1,3),(TURN((MOVE(T+1)),P),P=1,3),
     *                   (TURN((MOVE(T+2)),P),P=1,3),(TURN((M
     *                   OVE(T+2)),P),P=1,3)
1084      FORMAT ('0',12X,'TIME',6X,'APPROACH VOLUME',
     *            1X,2('#',3A4,1X,'%',3A4,1X),'#',3A4,1X,'%'
     *            ,3A4,'%TRKS',2X,'PHF',/,24X,'IN',2X,'OUT',
     *            2X,'TOTAL')
          GO TO 774
773       WRITE (PRTFIL, 1085)
1085      FORMAT ('0',12X,'TIME',17X,'APPROACH VOLUME',7X,
     *            '%TRKS',8X,'PHF',/,35X,'IN',2X,'OUT',2X,
     *            'TOTAL')
C
774       DO 922 I=1,SUB
            GO TO (9999,9999,777,778,779), NINT
777         WRITE (PRTFIL, 1086) I,SPTIME(I),ESPTIM(I),LAGHR(L,I),
     *                    LAGOUT(L,I),INDRUN(L,I),PERMON(L,1,
     *                     I),MOVPER(L,1,I),PERMON(L,2,I),MOV
     *                     PER(L,2,I),INDTRK(L,I),PHF(L,I)
1086        FORMAT (' ',I1,' PEAK',2X,F5.2,'-',F5.2,3X,I4,1X,
     *              I4,I7,6X,I5,10X,F6.2,9X,I5,12X,F6.2,8X,
     *            F6.2,4X,F4.2)
            GO TO 922
778         WRITE (PRTFIL, 1087) I,SPTIME(I),ESPTIM(I),LAGHR(L,I),
     *                     LAGOUT(L,I),INDRUN(L,I),PERMON(L,1
     *                     ,I),MOVPER(L,1,I),PERMON(L,2,I),MO
     *                    VPER(L,2,I),PERMON(L,3,I),MOVPER(L,
     *                     3,I),INDTRK(L,I),PHF(L,I)
1087        FORMAT (' ',I1,' PEAK',2X,F5.2,'-',F5.2,2X,I4,1X,
     *            I4,I7,7X,I5,8X,F6.2,9X,I5,7X,F6.2,8X,I5,7X,
     *              F6.2,4X,F6.2,2X,F4.2)
            GO TO 922
779         WRITE (PRTFIL, 1088) I,SPTIME(I),ESPTIM(I),LAGHR(L,I),
     *                     LAGOUT(L,I),INDRUN(L,I),INDTRK(L,I
     *                     ),PHF(L,I)
1088        FORMAT (' ',I1,' PEAK',2X,F5.2,'-',F5.2,13X,I4,1X
     *              ,I4,1X,I6, 6X,F6.2,7X,F4.2)
922       CONTINUE
C
          IF (NINT .NE. 5) GO TO 784
            T=1
            WRITE (PRTFIL, 1089) (TURN((MOVE(T)),P),P=1,3),(TURN(
     *                     (MOVE(T)),P),P=1,3),(TURN((MOVE(T
     *                     +1)),P),P=1,3),(TURN((MOVE(T+1)),
     *                     P),P=1,3),(TURN((MOVE(T+2)),P),P=
     *                     1,3),(TURN((MOVE(T+2)),P),P=1,3),
     *                     (TURN((MOVE(T+3)),P),P=1,3),(TURN
     *                     ((MOVE(T+3)),P),P=1,3)
1089        FORMAT ('0',12X,'TIME',4X,3('#',3A4,1X,'%',3A4,1X
     *              ),'#',3A4,1X,'%',3A4)
            DO 923 I=1,SUB
              WRITE (PRTFIL, 1090) I,SPTIME(I),ESPTIM(I),PERMON(L,1
     *                       ,I),MOVPER(L,1,I),PERMON(L,2,I),
     *                       MOVPER(L,2,I),PERMON(L,3,I),MOVP
     *                       ER(L,3,I),PERMON(L,4,I),MOVPER(L
     *                       ,4,I)
1090          FORMAT (' ',I1,' PEAK',2X,F5.2,'-',F5.2,6X,3(I5
     *                ,8X,F6.2,9X),I5,8X,F6.2)
923         CONTINUE
784       T=T+(NINT-1)
921     CONTINUE
C
C THE PRINTING OF THE PAVEMENT DESIGN SUMMARY FOR THE INTER-
C SECTION WHICH INCLUDES THE ANNUAL AVERAGE DAILY TRAFFIC,
C THE ANNUAL AVERAGE DAILY TRUCK TRAFFIC, VOLUME OF EACH
C CLASSIFICATION, AND THE PERCENTAGE OF THAT VOLUME COMPARED
C TO THE TOTAL VOLUME OF AN APPROACHING STREET FOR APPROACH-
C ING BOTH THE INTERSECTION AND AN APPROACHING STREET.
C
        PAGE = PAGE + 1
        WRITE (PRTFIL, 1000) SYDATE, PAGE, SYTIME
        WRITE (PRTFIL, 1091)
1091    FORMAT ('0',53X,'PAVEMENT DESIGN SUMMARY')
        WRITE (PRTFIL, 1001) SITEID, (LOC(J),J=1,20),YY,MM,DD,
     1               (CREW(J),J=1,3), (DOW(J),J=1,3), (W1(I),I=1,2),
     2                 INDXNO
        DO 801 L=1,NINT
            WRITE (PRTFIL, 1092) (STREET(L,I),I=1,5), SECTID(L),
     1             PCSLEG(L)
1092        FORMAT ('0',/,1X,'LEG: ',5A4,5X,'SUBSECTION ID: ',A6,
     1              5X,'PCS MATCH: ',A6/,36X,'AADT',4X,'AA',            ITMS1.00
     *       'DTT',4X,'#CAR/PU',3X,'%CAR/PU',3X,'#SU/TRK',3X,
     *              '%SU/TRK',4X,'#TT3',4X,'%TT3',4X,'#TT6',4
     *              X,'%TT6',/,54X,'VAN/MC',4X,'VAN/MC',4X,
     *              'BUSES',5X,'BUSES',6X,'4,5',5X,'4,5')
            WRITE (PRTFIL, 1093) AADTIN(L),AADTT1(L),VTDIR1(L,1),DI
     *                     R1(L,1),VTDIR1(L,2),DIR1(L,2),VTDI
     *                     R1(L,3),DIR1(L,3),VTDIR1(L,4),DIR1
     *                     (L,4)
1093        FORMAT ('0','APPROACHING INTERSECTION:',7X,I7,2X,
     *              I7,4X,I7,4X,F6.2,3X,I5,5X,F6.2,5X,I4,2X,
     *              F6.2,3X,I4,3X,F6.2)
            WRITE (PRTFIL, 1094) AADTOT(L),AADTT2(L),VTDIR2(L,1),DI
     *                     R2(L,1),VTDIR2(L,2),DIR2(L,2),VTDI
     *                     R2(L,3),DIR2(L,3),VTDIR2(L,4),DIR2
     *                     (L,4)
1094        FORMAT (' ','LEAVING INTERSECTION:',11X,I7,2X,I7,
     *              4X,I7,4X,F6.2,3X,I5,5X,F6.2,5X,I4,2X,F6.2
     *              ,3X,I4,3X,F6.2)
            WRITE (PRTFIL, 1095) TOAADT(L),TAADTT(L),TOTVT(L,1),DIR
     *                     TOT(L,1),TOTVT(L,2),DIRTOT(L,2),TO
     *                     TVT(L,3),DIRTOT(L,3),TOTVT(L,4),DI
     *                     RTOT(L,4)
1095        FORMAT (' ','TOTAL',27X,I7,2X,I7,4X,I7,4X,F6.2,2X
     *              ,I6, 5X,F6.2,4X,I5,2X,F6.2,2X,I5,3X,F6.2)
801     CONTINUE
C
C       EJECT PRINTER PAGE AT END OF JOB ...
C
9999  WRITE (PRTFIL,10000)                                              REV.1.02
10000 FORMAT ('1')                                                      REV.1.02
      RETURN                                                            REV.1.02
      END
